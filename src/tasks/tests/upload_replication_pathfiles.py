import os
import glob
from datetime import datetime
import time

from rucio.client.uploadclient import Client, UploadClient

from elasticsearch import Elasticsearch

from common.rucio.helpers import createCollection
from tasks.task import Task
from utility import bcolors


class TestUploadReplicationPathfiles(Task):
    """ Rucio file upload/replication files in a path. """

    def __init__(self, logger):
        super().__init__(logger)
        self.activity = None
        self.rses = None
        self.scope = None
        self.lifetime = None
        self.pathfiles = None
        self.outputDatabases = None
        self.taskName = None
        self.namingPrefix = None

    def run(self, args, kwargs):
        super().run()
        self.tic()
        try:
            self.activity = kwargs["activity"]
            self.rses = kwargs["rses"]
            self.scope = kwargs["scope"]
            self.lifetime = kwargs["lifetime"]
            self.pathfiles=kwargs["pathfiles"]
            self.outputDatabases = kwargs["output"]["databases"]
            self.taskName = kwargs["task_name"]
            self.namingPrefix = kwargs.get("naming_prefix", "")
        except KeyError as e:
            self.logger.critical("Could not find necessary kwarg for task.")
            self.logger.critical(repr(e))
            return False

        # Create a dataset to house the data, named with today's date
        # and scope <scope>.
        #
        datasetDID = createCollection(self.logger.name, self.scope)

        # Upload a file of size from <sizes> to a random RSE, attach to 
        # the dataset and add replication rules to another random RSE.
        #
        
        rseSrc = self.rses[0]
        rseDst = self.rses[1:]
        
        entries=[]
        for p in self.pathfiles:
            
            path = p["path"]
            self.logger.info("Path file: {}".format(path))
            
            #check if file or folder
            lFiles=[]
            if os.path.isfile(path):
                lFiles = [path]
                
            elif  os.path.isdir(path):  
                pattern=p["pattern"]
                lFiles = glob.glob("{}/{}".format(path, pattern))
            
            nFiles = len(lFiles)     
            for idx in range(nFiles):
                
                f = lFiles[idx]
                fname = os.path.basename(f)
                fileDID = "{}:{}".format(self.scope, fname)
                size = os.path.getsize(f)
                
                self.logger.info(
                bcolors.OKBLUE + "RSE (src): {}".format(rseSrc) + bcolors.ENDC
                )
            
                self.logger.debug("File size: {} bytes".format(size))

                # Upload to <rseSrc>
                self.logger.debug("Uploading file {} of {}".format(idx +1, 
                                                                  self.nFiles))
                
                now = datetime.now()
                entry = {
                    "task_name": self.taskName,
                    "scope": self.scope,
                    "name": f,
                    "file_size": size,
                    "type": "file",
                    "n_files": 1,
                    "to_rse": rseSrc,
                    "attempted_at": now.isoformat(),
                    "is_upload_submitted": 1,
                }
                
                try:
                    
                    st = time.time()
 
                    items = [{
                    "path": f,
                    "rse": rseSrc,
                    "did_scope": self.scope,
                    "lifetime": self.lifetime,
                    "register_after_upload": True,
                    "force_scheme": None,
                    "transfer_timeout": 60,
                    }]
                    client = UploadClient(logger=self.logger)
                    client.upload(items=items)
                    
                    # Add keys for successful upload.
                    entry["transfer_duration"] = time.time() - st
                    entry["transfer_overhead"] = entry["file_size"] / (entry["transfer_duration"]*1000)
                    entry["state"] = "UPLOAD-SUCCESSFUL"
                    entry["is_upload_successful"] = 1
                    self.logger.debug("Upload complete")

                    # Attach to dataset
                    self.logger.debug(
                        "Attaching file {} to {}".format(fileDID, datasetDID)
                    )
                    try:
                        client = Client(logger=self.logger)
                        tokens = datasetDID.split(":")
                        toScope = tokens[0]
                        toName = tokens[1]
                        attachment = {"scope": toScope, "name": toName, 
                                      "dids": []}
                        for did in fileDID.split(" "):
                            tokens = fileDID.split(":")
                            scope = tokens[0]
                            name = tokens[1]
                            attachment["dids"].append({"scope": scope, 
                                                       "name": name})
                        client.attach_dids_to_dids(attachments=[attachment])
                        self.logger.debug("Attached file to dataset")        
                    except Exception as e:
                        self.logger.warning(repr(e))
               
                except Exception as e:
                    self.logger.warning("Upload failed: {}".format(e))
                    
                    # Add keys for failed upload.
                    entry["error"] = repr(e.__class__.__name__).strip("'")
                    entry["error_details"] = repr(e).strip("'")
                    entry["state"] = "UPLOAD-FAILED"
                    entry["is_upload_failed"] = 1
                  
                entries.append(entry)
                    
                
                # Add replication rules for other RSEs
                self.logger.debug("Adding replication rules...")
                for rse in rseDst:
                    
                                       
                    self.logger.debug(bcolors.OKGREEN
                        + "RSE (dst): {}".format(rse)
                        + bcolors.ENDC
                    )
                    
                    now = datetime.now()
                    entry = {
                            "task_name": self.taskName,
                            "scope": self.scope,
                            "name": f,
                            "file_size": size,
                            "type": "file",
                            "n_files": 1,
                            "to_rse": rse,
                            "attempted_at": now.isoformat(),
                            "is_upload_submitted": 1,                   
                        }

                    try:
                        
                        st = time.time()
                        
                        tokens = fileDID.split(":")
                        scope = tokens[0]
                        name = tokens[1]

                        client = Client(logger=self.logger)
                        rtn = client.add_replication_rule(
                            dids=[{"scope": scope, "name": name}],
                            copies=1,
                            rse_expression=rse,
                            lifetime=self.lifetime,
                            activity=self.activity,
                            source_replica_expression=rseSrc,
                            asynchronous=False,
                        )
                        
                        # Add keys for successful upload.
                        entry["transfer_duration"] = time.time() - st
                        entry["transfer_overhead"] = entry["file_size"] / (entry["transfer_duration"]*1000)
                        entry["state"] = "REPLIED-SUCCESSFUL"
                        entry["is_replied_successful"] = 1
                        
                        self.logger.debug("Rule ID: {}".format(rtn[0]))
                        self.logger.debug("Replication rules added")
                    except Exception as e:
                        self.logger.warning(repr(e))
                        
                        # Add keys for failed upload.
                        entry["error"] = repr(e.__class__.__name__).strip("'")
                        entry["error_details"] = repr(e).strip("'")
                        entry["state"] = "REPLIED-FAILED"
                        entry["is_replied_failed"] = 1
                        
                    entries.append(entry)     
                    
                
        # Push task output to databases.
        #
        if self.outputDatabases is not None:
            for database in self.outputDatabases:
                if database["type"] == "es":
                    self.logger.info("Sending output to ES database...")
                    es = Elasticsearch([database['uri']])
                    for entry in entries:
                        es.index(index=database["index"], id=entry['name'], body=entry)

        self.toc()
        self.logger.info("Finished in {}s".format(round(self.elapsed)))
